/**
 * In this testcase, callParamMethod take a param with type A.
 * callParamMethod was called twice. Once with param a = not-null,
 * the other time with a = don't know. When solving the constraint,
 * we should be able to conclude param a of callParamMethod is don't know,
 * therefore a.foo2() may raise a null pointer exception.
 *
 */

class Main {
    public static void main(String[] a){
        System.out.println(new A().foo1());
    }
}

class A {
    B f;
    A f2;
    public int foo1(){
        int i1;
        i1 = 3;
        f = new B();
        System.out.println(f.callParamMethod(this));
        System.out.println(f.callParamMethod(f2));
        return 1;
    }

    public int foo2(){
        return 5;
    }

}

class B extends A{
    public int callParamMethod(A a){
        return a.foo2();
    }
}