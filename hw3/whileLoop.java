class Main {
    public static void main(String[] a){
    	int iter;
    	int initValue;
    	A a;

    	initValue = 5;
    	a = new A();
    	iter = a.init(initValue);
    	while (!a.endLoop()){
    		iter = a.decrement();
    	} 

        System.out.println(iter); //iter should be -1 at this point
    }
}

class A {
	int check;

	public int init(int initValue){
		check = initValue;
		return initValue;
	}
	public boolean endLoop(){
		if (check < 0){
			return true;
		}
		return false; 
	}

   	public int decrement(){
   		check = check - 1; 
   		return check;
   	}

    public 
}
