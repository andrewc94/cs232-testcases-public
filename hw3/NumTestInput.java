// this program theoretically prints out all positive numbers (42, every time), but if a negative number is fed in as input it will print that negative number.
// the numerical analyzer should be able to catch that a negative number will be printed.
class Main {
    public static void main(String[] a){
		System.out.println(new Num().setUp(-11));
        System.out.println(new Num().setUp(25));
		System.out.println(new Num().setUp(42));
    }
}

class Num {
	int n;
	
	public int setUp(int i) {
		n = i;
		if (n < 42) {
			addUp();
		} else {
			subDown();
		}
		return n;
	}
	
	public void addUp() {
		if (n < 0) {
			System.out.print(n);
		}
		while (n < 42) {
			n = n + 1;
		}
	}
	
	public void subDown() {
		while (42 < n) {
			n = n - 1;
		}
	}
}